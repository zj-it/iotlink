package com.yunze.common.mapper.yunze.backup;

import java.util.List;
import java.util.Map;

public interface BackupMapper {
    public List<Map<String, Object>> GenTableList();

    public List<Map<String, Object>> GenTableColumnList();

    public List<Map<String, Object>> QrtzBlobTriggersList();

    public List<Map<String, Object>> QrtzCalendarsList();

    public List<Map<String, Object>> QrtzCronTriggersList();

    public List<Map<String, Object>> QrtrootiredTriggersList();

    public List<Map<String, Object>> QrtzJobDetailsList();

    public List<Map<String, Object>> QrtzLocksList();

    public List<Map<String, Object>> QrtzPausedTriggerGrpsList();

    public List<Map<String, Object>> QrtzSchedulerStateList();

    public List<Map<String, Object>> QrtzSimpleTriggersList();

    public List<Map<String, Object>> QrtzSimpropTriggersList();

    public List<Map<String, Object>> QrtzTriggersList();

    public List<Map<String, Object>> SysConfigList();

    public List<Map<String, Object>> SysDeptList();

    public List<Map<String, Object>> SysDictDataList();

    public List<Map<String, Object>> SysDictTypeList();

    public List<Map<String, Object>> SysJobList();

    public List<Map<String, Object>> SysJobLogList();

    public List<Map<String, Object>> SysLogininforList();

    public List<Map<String, Object>> SysMenuList();

    public List<Map<String, Object>> SysNoticeList();

    public List<Map<String, Object>> SysOperLogList();

    public List<Map<String, Object>> SysPostList();

    public List<Map<String, Object>> SysRoleList();

    public List<Map<String, Object>> SysRoleDeptList();

    public List<Map<String, Object>> SysRoleMenuList();

    public List<Map<String, Object>> SysUserList();

    public List<Map<String, Object>> SysUserPostList();

    public List<Map<String, Object>> SysUserRoleList();

    public List<Map<String, Object>> YzAgentAccountList();

    public List<Map<String, Object>> YzAgentPackageList();

    public List<Map<String, Object>> YzAgentPacketList();

    public List<Map<String, Object>> YzCardFlowList();

    public List<Map<String, Object>> YzCardFlowHisList();

    public List<Map<String, Object>> YzCardInfoList();

    public List<Map<String, Object>> YzCardOperatingList();

    public List<Map<String, Object>> YzCardPackageList();

    public List<Map<String, Object>> YzCardPacketList();

    public List<Map<String, Object>> YzCardRouteList();

    public List<Map<String, Object>> YzExecutionTaskList();

    public List<Map<String, Object>> YzOrderList();

    public List<Map<String, Object>> YzPassagewayPollingList();

    public List<Map<String, Object>> YzWechatUserList();

    public List<Map<String, Object>> YzWxAutoreplyList();

    public List<Map<String, Object>> YzWxConfigList();

    public List<Map<String, Object>> YzWxMenuList();




}


















































