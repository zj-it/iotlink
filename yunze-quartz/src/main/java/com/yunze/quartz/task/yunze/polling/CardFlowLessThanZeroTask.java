package com.yunze.quartz.task.yunze.polling;

import com.alibaba.fastjson.JSON;
import com.yunze.apiCommon.mapper.YzCardRouteMapper;
import com.yunze.common.mapper.yunze.YrootlowHisMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日用量记录为 负数 卡号 再次同步用量
 * @Auther: zhang feng
 * @Date: 2022/04/21/12:22
 * @Description:
 */
@Component("cardFlowLessThanZeroTask")
public class CardFlowLessThanZeroTask {


    @Resource
    private YzCardRouteMapper yzCardRouteMapper;
    @Resource
    private RabbitTemplate rabbitTemplate;
    @Resource
    private YrootlowHisMapper yrootlowHisMapper;


    public void pollingCardFlowLessThanZero(Integer time) {
        //1.状态 正常 轮询开启 时 获取  每个 通道下卡号 加入队列
        Map<String, Object> findRouteID_Map = new HashMap<>();
        List<String> lArr =  yrootlowHisMapper.lessThanZeroChannelId(null);
        if(lArr!=null && lArr.size()>0){
            findRouteID_Map.put("Cd_idArr", lArr);
            List<Map<String, Object>> channelArr = yzCardRouteMapper.findRouteID(findRouteID_Map);
            if (channelArr != null && channelArr.size() > 0) {
                String CardFlow_routingKey = "polling.cardCardFlowLessThanZero.routingKey";
                //2.获取 通道下卡号
                for (int i = 0; i < channelArr.size(); i++) {
                    Map<String, Object> channel_obj = channelArr.get(i);
                    Map<String, Object> findMap = new HashMap<>();
                    String cd_id = channel_obj.get("cd_id").toString();
                    findMap.put("channel_id", cd_id);

                    List<Map<String, Object>> cardArr = yrootlowHisMapper.findChannelIdLessThanZero(findMap);
                    if (cardArr != null && cardArr.size() > 0) {
                         pollingExecute(cardArr,channel_obj,CardFlow_routingKey,time);
                    }
                }
            }
        }
    }


    /**
     * 公共执行
     * @param cardArr
     * @param channel_obj
     * @param CardFlow_routingKey
     * @param time
     */
    public void pollingExecute(List<Map<String, Object>> cardArr,Map<String, Object> channel_obj,String CardFlow_routingKey,Integer time){
        //卡号放入路由
        for (int j = 0; j < cardArr.size(); j++) {
            Map<String, Object> card = cardArr.get(j);
            Map<String, Object> Card = new HashMap<>();
            Card.putAll(channel_obj);
            Card.put("iccid", card.get("iccid"));
            Card.put("card_no", card.get("card_no"));
            Card.put("network_type", card.get("network_type"));
            String msg = JSON.toJSONString(Card);
            //生产任务
            try {
                rabbitTemplate.convertAndSend("polling_cardCardFlow_exchange", CardFlow_routingKey, msg, message -> {
                    // 设置消息过期时间 time 分钟 过期
                    message.getMessageProperties().setExpiration("" + (time * 1000 * 60));
                    return message;
                });
            } catch (Exception e) {
                System.out.println(e.getMessage().toString());
            }
        }
    }




}
