package com.yunze.quartz.task.yunze.cardflow;

import com.yunze.apiCommon.mapper.YzCardRouteMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 卡通道表 卡总数 定时任务【同步更改字段数据】
 */
@Slf4j
@Component("cardCount")
public class CardCount {

    @Resource
    private YzCardRouteMapper yzCardRouteMapper;

    public void synchronization() {

        Map<String, Object> Rmap = new HashMap<>();

        List<Map<String, Object>> list = yzCardRouteMapper.countId(Rmap);//查询 通道卡总数
        if (list != null) {
            try {
                for (int i = 0; i < list.size(); i++) {
                    Map<String, Object> obj = list.get(i);
                    if (obj.get("channel_id") != null) {
                       Integer integer = yzCardRouteMapper.upCount(obj); //进行修改 通道表 卡总数
                        log.info(integer +"数据操作成功");
                    }
                }
            } catch (Exception e) {
                System.out.println("操作异常" + e.getMessage());
            }
        }
    }
}

