package com.yunze.system.card;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.yunze.common.core.redis.RedisCache;
import com.yunze.common.mapper.yunze.backup.BackupMapper;
import com.yunze.common.utils.yunze.VeDate;
import com.yunze.common.utils.yunze.WriteCSV;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 导出 消费 者
 */
@Slf4j
@Component
public class GenTable {


    @Resource
    private RedisCache redisCache;
    @Resource
    private WriteCSV writeCSV;
    @Resource
    private BackupMapper genTableMapper;

    @RabbitHandler
    @RabbitListener(queues = "admin_BackupsGenTable_queue")
    public void DbCard(String msg, Channel channel) throws IOException {
        try {
            if (StringUtils.isEmpty(msg)) {
                return;
            }
            Map<String, Object> Bmap = JSON.parseObject(msg);
            //System.out.println(Bmap);
            String create_by = Bmap.get("Masage").toString();//上传新文件名
            String prefix = "admin_BackupsGenTable_queue";
            //执行前判断 redis 是否存在 执行数据 存在时 不执行
            Object isExecute = redisCache.getCacheObject(prefix + ":" + create_by);
            if (isExecute == null) {
                redisCache.setCacheObject(prefix + ":" + create_by, msg, 3, TimeUnit.SECONDS);//3 秒缓存 避免 重复消费
                GenTabl();
                gentablecolumn();
                qrtzblobtriggers();
                qrtzcrontriggers();
                qrtzcalendars();
                qrtrootiredtriggers();
                qrtzjobdetails();
                qrtzlocks();
                qrtzpausedtriggergrps();
                qrtzschedulerstate();
                qrtzsimpletriggers();
                qrtzsimproptriggers();
                qrtztriggers();
                sysconfig();
                sysdept();
                sysdictdata();
                sysdicttype();
                sysjob();
                sysjoblog();
                syslogininfor();
                sysmenu();
                sysnotice();
                sysoperlog();
                syspost();
                sysrole();
                sysroledept();
                sysrolemenu();
                sysuser();
                sysuserpost();
                sysuserrole();
                yzagentaccount();
                yzagentpackage();
                yzagentpacket();
                yzcardflow();
                yzcardinfo();
                yzcardoperating();
                yzcardpackage();
                yzcardpacket();
                yzcardroute();
                yzexecutiontask();
                yzorder();
                yzpassagewaypolling();
                yzwechatuser();
                yzwxautoreply();
                yzwxconfig();
                yzwxmenu();
            }
        } catch (Exception e) {
            log.error(">>错误 - 连接管理导出 消费者:{}<<", e.getMessage().toString());
        }
    }


    /**
     * 备份 GenTabl
     */
    public void GenTabl() {

        //1.查询出 yz_sys_logs
        List<Map<String, Object>> channelArr = genTableMapper.GenTableList(); //查询所有
        System.out.println("备份 GenTable ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_GenTable";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"table_id", "table_name", "table_comment", "sub_table_name", "sub_table_fk_name", "class_name",
                "tpl_category", "package_name", "module_name", "business_name", "function_name", "function_author", "gen_type",
                "gen_path", "options", "create_by", "create_time", "update_by", "update_time", "remark"};//数据库的注释名
        String keys[] = {"table_id", "table_name", "table_comment", "sub_table_name", "sub_table_fk_name", "class_name",
                "tpl_category", "package_name", "module_name", "business_name", "function_name", "function_author", "gen_type",
                "gen_path", "options", "create_by", "create_time", "update_by", "update_time", "remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 GenTableColumn
     */
    public void gentablecolumn() {
        List<Map<String, Object>> channelArr = genTableMapper.GenTableColumnList(); //查询所有
        System.out.println("备份 GenTableColumn ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_GenTableColumn";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"column_id", "table_id", "column_name", "column_comment", "column_type", "java_type", "java_field",
                "is_pk", "is_increment", "is_required", "is_insert", "is_edit", "is_list", "is_query,query_type",
                "html_type", "dict_type", "sort", "create_by", "create_time", "update_by", "update_time"};//数据库的注释名
        String keys[] = {"column_id", "table_id", "column_name", "column_comment", "column_type", "java_type", "java_field",
                "is_pk", "is_increment", "is_required", "is_insert", "is_edit", "is_list", "is_query,query_type",
                "html_type", "dict_type", "sort", "create_by", "create_time", "update_by", "update_time"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 QrtzBlobTriggers
     */
    public void qrtzblobtriggers() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzBlobTriggersList(); //查询所有
        System.out.println("备份 QrtzBlobTriggers ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzBlobTriggers";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "trigger_name", "trigger_group", "blob_data"};//数据库的注释名
        String keys[] = {"sched_name", "trigger_name", "trigger_group", "blob_data"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzCalendars
     */
    public void qrtzcalendars() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzCalendarsList(); //查询所有
        System.out.println("备份 QrtzCalendars ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzCalendars";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "calendar_name", "calendar"};//数据库的注释名
        String keys[] = {"sched_name", "calendar_name", "calendar"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 QrtzCronTriggers
     */
    public void qrtzcrontriggers() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzCronTriggersList(); //查询所有
        System.out.println("备份 QrtzCronTriggers ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzCronTriggers";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "trigger_name", "trigger_group", "cron_expression", "time_zone_id"};//数据库的注释名
        String keys[] = {"sched_name", "trigger_name", "trigger_group", "cron_expression", "time_zone_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtrootiredTriggers
     */
    public void qrtrootiredtriggers() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtrootiredTriggersList(); //查询所有
        System.out.println("备份 QrtrootiredTriggersList ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtrootiredTriggers";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "entry_id", "trigger_name", "trigger_group", "instance_name", "fired_time", "sched_time", "priority",
                "state", "job_name", "job_group", "is_nonconcurrent", "requests_recovery"};//数据库的注释名
        String keys[] = {"sched_name", "entry_id", "trigger_name", "trigger_group", "instance_name", "fired_time", "sched_time", "priority",
                "state", "job_name", "job_group", "is_nonconcurrent", "requests_recovery"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzJobDetails
     */
    public void qrtzjobdetails() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzJobDetailsList(); //查询所有
        System.out.println("备份 QrtzJobDetails ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzJobDetails";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "job_name", "job_group", "description", "job_class_name", "is_durable", "is_nonconcurrent",
                "is_update_data", "requests_recovery", "job_data"};//数据库的注释名
        String keys[] = {"sched_name", "job_name", "job_group", "description", "job_class_name", "is_durable", "is_nonconcurrent",
                "is_update_data", "requests_recovery", "job_data"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzLocks
     */
    public void qrtzlocks() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzLocksList(); //查询所有
        System.out.println("备份 QrtzLocks ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzLocks";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "lock_name"};//数据库的注释名
        String keys[] = {"sched_name", "lock_name"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzPausedTriggerGrps
     */
    public void qrtzpausedtriggergrps() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzPausedTriggerGrpsList(); //查询所有
        System.out.println("备份 QrtzPausedTriggerGrps ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzPausedTriggerGrps";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "trigger_group"};//数据库的注释名
        String keys[] = {"sched_name", "trigger_group"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzSchedulerState
     */
    public void qrtzschedulerstate() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzSchedulerStateList(); //查询所有
        System.out.println("备份 QrtzSchedulerState ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzSchedulerState";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "instance_name", "last_checkin_time", "checkin_interval"};//数据库的注释名
        String keys[] = {"sched_name", "instance_name", "last_checkin_time", "checkin_interval"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzSimpleTriggers
     */
    public void qrtzsimpletriggers() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzSimpleTriggersList(); //查询所有
        System.out.println("备份 QrtzSimpleTriggers ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzSimpleTriggers";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "trigger_name", "trigger_group", "repeat_count", "repeat_interval", "times_triggered"};//数据库的注释名
        String keys[] = {"sched_name", "trigger_name", "trigger_group", "repeat_count", "repeat_interval", "times_triggered"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 QrtzSimpropTriggers
     */
    public void qrtzsimproptriggers() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzSimpropTriggersList(); //查询所有
        System.out.println("备份 QrtzSimpropTriggers ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzSimpropTriggers";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name", "trigger_name", "trigger_group", "str_prop_1", "str_prop_2", "str_prop_3", "int_prop_1", "int_prop_2",
                "long_prop_1", "long_prop_2", "dec_prop_1", "dec_prop_2", "bool_prop_1", "bool_prop_1"};//数据库的注释名
        String keys[] = {"sched_name", "trigger_name", "trigger_group", "str_prop_1", "str_prop_2", "str_prop_3", "int_prop_1", "int_prop_2",
                "long_prop_1", "long_prop_2", "dec_prop_1", "dec_prop_2", "bool_prop_1", "bool_prop_1"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出

    }

    /**
     * 备份 QrtzTriggers
     */
    public void qrtztriggers() {
        List<Map<String, Object>> channelArr = genTableMapper.QrtzTriggersList(); //查询所有
        System.out.println("备份 QrtzTriggers ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_QrtzTriggers";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"sched_name","trigger_name","trigger_group","job_name","job_group","description","next_fire_time",
                "prev_fire_time","priority","trigger_state","trigger_type","start_time","end_time","calendar_name",
                "misfire_instr","job_data"};//数据库的注释名
        String keys[] = {"sched_name","trigger_name","trigger_group","job_name","job_group","description","next_fire_time",
                "prev_fire_time","priority","trigger_state","trigger_type","start_time","end_time","calendar_name",
                "misfire_instr","job_data"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
    * 备份 SysConfig
    */
    public void sysconfig() {
        List<Map<String, Object>> channelArr = genTableMapper.SysConfigList(); //查询所有
        System.out.println("备份 SysConfig ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysConfig";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by",
                "update_time","remark"};//数据库的注释名
        String keys[] = {"config_id","config_name","config_key","config_value","config_type","create_by","create_time","update_by",
                "update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
    * 备份 SysDept
    */
    public void sysdept() {
        List<Map<String, Object>> channelArr = genTableMapper.SysDeptList(); //查询所有
        System.out.println("备份 SysDept ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysDept";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by",
                "create_time","update_by","update_time"};//数据库的注释名
        String keys[] = {"dept_id","parent_id","ancestors","dept_name","order_num","leader","phone","email","status","del_flag","create_by",
                "create_time","update_by","update_time"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysDictData
     */
    public void sysdictdata() {
        List<Map<String, Object>> channelArr = genTableMapper.SysDictDataList(); //查询所有
        System.out.println("备份 SysDictData ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysDictData";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status",
                "create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"dict_code","dict_sort","dict_label","dict_value","dict_type","css_class","list_class","is_default","status",
                "create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysDictType
     */
    public void sysdicttype() {
        List<Map<String, Object>> channelArr = genTableMapper.SysDictTypeList(); //查询所有
        System.out.println("备份 SysDictType ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysDictType";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"dict_id","dict_name","dict_type","status","create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"dict_id","dict_name","dict_type","status","create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysJob
     */
    public void sysjob() {
        List<Map<String, Object>> channelArr = genTableMapper.SysJobList(); //查询所有
        System.out.println("备份 SysJob ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysJob";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"job_id","job_name","job_group","invoke_target","cron_expression","misfire_policy","concurrent",
                "status","create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"job_id","job_name","job_group","invoke_target","cron_expression","misfire_policy","concurrent",
                "status","create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysJobLog
     */
    public void sysjoblog() {
        List<Map<String, Object>> channelArr = genTableMapper.SysJobLogList(); //查询所有
        System.out.println("备份 SysJobLog ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysJobLog";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"job_log_id","job_name","job_group","invoke_target","job_message","status","exception_info","create_time"};//数据库的注释名
        String keys[] = {"job_log_id","job_name","job_group","invoke_target","job_message","status","exception_info","create_time"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysLogininfor
     */
    public void syslogininfor() {
        List<Map<String, Object>> channelArr = genTableMapper.SysLogininforList(); //查询所有
        System.out.println("备份 SysLogininfor ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysLogininfor";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"info_id","user_name","ipaddr","login_location","browser","os","status","msg","login_time"};//数据库的注释名
        String keys[] = {"info_id","user_name","ipaddr","login_location","browser","os","status","msg","login_time"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysMenu
     */
    public void sysmenu() {
        List<Map<String, Object>> channelArr = genTableMapper.SysMenuList(); //查询所有
        System.out.println("备份 SysMenu ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysMenu";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"menu_id","menu_name","parent_id","order_num","path","component","is_frame","is_cache","menu_type","visible",
                "status","perms","icon","create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"menu_id","menu_name","parent_id","order_num","path","component","is_frame","is_cache","menu_type","visible",
                "status","perms","icon","create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysNotice
     */
    public void sysnotice() {
        List<Map<String, Object>> channelArr = genTableMapper.SysNoticeList(); //查询所有
        System.out.println("备份 SysNotice ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysNotice";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"notice_id","notice_title","notice_type","notice_content","status","create_by","create_time","update_by",
                "update_time","remark"};//数据库的注释名
        String keys[] = {"notice_id","notice_title","notice_type","notice_content","status","create_by","create_time","update_by",
                "update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysOperLog
     */
    public void sysoperlog() {
        List<Map<String, Object>> channelArr = genTableMapper.SysOperLogList(); //查询所有
        System.out.println("备份 SysOperLog ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysOperLog";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"oper_id","title","business_type","method","request_method","operator_type","oper_name","dept_name","oper_url",
                "oper_ip","oper_location","oper_param","json_result","status","error_msg","oper_time"};//数据库的注释名
        String keys[] = {"oper_id","title","business_type","method","request_method","operator_type","oper_name","dept_name","oper_url",
                "oper_ip","oper_location","oper_param","json_result","status","error_msg","oper_time"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysPost
     */
    public void syspost() {
        List<Map<String, Object>> channelArr = genTableMapper.SysPostList(); //查询所有
        System.out.println("备份 SysPost ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysPost";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"post_id","post_code","post_name","post_sort","status","create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"post_id","post_code","post_name","post_sort","status","create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysRole
     */
    public void sysrole() {
        List<Map<String, Object>> channelArr = genTableMapper.SysRoleList(); //查询所有
        System.out.println("备份 SysRole ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysRole";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"role_id","role_name","role_key","role_sort","data_scope","menu_check_strictly","dept_check_strictly","status",
                "del_flag","create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"role_id","role_name","role_key","role_sort","data_scope","menu_check_strictly","dept_check_strictly","status",
                "del_flag","create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysRoleDept
     */
    public void sysroledept() {
        List<Map<String, Object>> channelArr = genTableMapper.SysRoleDeptList(); //查询所有
        System.out.println("备份 SysRoleDept ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysRoleDept";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"role_id","dept_id"};//数据库的注释名
        String keys[] = {"role_id","dept_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysRoleMenu
     */
    public void sysrolemenu() {
        List<Map<String, Object>> channelArr = genTableMapper.SysRoleMenuList(); //查询所有
        System.out.println("备份 SysRoleMenu ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysRoleMenu";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"role_id","menu_id"};//数据库的注释名
        String keys[] = {"role_id","menu_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysUser
     */
    public void sysuser() {
        List<Map<String, Object>> channelArr = genTableMapper.SysUserList(); //查询所有
        System.out.println("备份 SysUser ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysUser";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"user_id","dept_id","user_name","nick_name","user_type","email","phonenumber","sex","avatar","password","status",
                "del_flag","login_ip","login_date","create_by","create_time","update_by","update_time","remark"};//数据库的注释名
        String keys[] = {"user_id","dept_id","user_name","nick_name","user_type","email","phonenumber","sex","avatar","password","status",
                "del_flag","login_ip","login_date","create_by","create_time","update_by","update_time","remark"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysUserPost
     */
    public void sysuserpost() {
        List<Map<String, Object>> channelArr = genTableMapper.SysUserPostList(); //查询所有
        System.out.println("备份 SysUserPost ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysUserPost";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"user_id","post_id"};//数据库的注释名
        String keys[] = {"user_id","post_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 SysUserRole
     */
    public void sysuserrole() {
        List<Map<String, Object>> channelArr = genTableMapper.SysUserRoleList(); //查询所有
        System.out.println("备份 SysUserRole ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysUserRole";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"user_id","role_id"};//数据库的注释名
        String keys[] = {"user_id","role_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 YzAgentAccount
     */
    public void yzagentaccount() {
        List<Map<String, Object>> channelArr = genTableMapper.YzAgentAccountList(); //查询所有
        System.out.println("备份 YzAgentAccount ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzAgentAccount";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","app_id","access_key","password","create_time","update_time","agent_name","agent_id","times","openurl","typesys"};//数据库的注释名
        String keys[] = {"id","app_id","access_key","password","create_time","update_time","agent_name","agent_id","times","openurl","typesys"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 YzAgentPackage
     */
    public void yzagentpackage() {
        List<Map<String, Object>> channelArr = genTableMapper.YzAgentPackageList(); //查询所有
        System.out.println("备份 YzAgentPackage ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzAgentPackage";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","package_id","package_agentname","package_name","create_time","error_so","dept_id","user_id"};//数据库的注释名
        String keys[] = {"id","package_id","package_agentname","package_name","create_time","error_so","dept_id","user_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 YzAgentPacket
     */
    public void yzagentpacket() {
        List<Map<String, Object>> channelArr = genTableMapper.YzAgentPacketList(); //查询所有
        System.out.println("备份 YzAgentPacket ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzAgentPacket";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","package_id","packet_id","packet_name","packet_wx_name","packet_price","packet_flow","base_packet_type",
                "packet_type","is_profit","show_profit","wechat_pay","balance_pay","create_time","error_flow","error_so",
                "packet_valid_time","packet_cost","is_month","date_limit","deduction","in_stock","packet_valid_type",
                "dept_id","user_id","packet_valid_name"};//数据库的注释名
        String keys[] = {"id","package_id","packet_id","packet_name","packet_wx_name","packet_price","packet_flow","base_packet_type",
                "packet_type","is_profit","show_profit","wechat_pay","balance_pay","create_time","error_flow","error_so",
                "packet_valid_time","packet_cost","is_month","date_limit","deduction","in_stock","packet_valid_type",
                "dept_id","user_id","packet_valid_name"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 YzCardFlow
     */
    public void yzcardflow() {
        List<Map<String, Object>> channelArr = genTableMapper.YzCardFlowList(); //查询所有
        System.out.println("备份 YzCardFlow ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzCardFlow";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","package_id","packet_id","ord_no","true_flow","error_flow","start_time","end_time","create_time","ord_type",
                "status","packet_type","use_flow","iccid","error_time","validate_type","validate_time","use_true_flow",
                "use_so_flow"};//数据库的注释名
        String keys[] = {"id","package_id","packet_id","ord_no","true_flow","error_flow","start_time","end_time","create_time","ord_type",
                "status","packet_type","use_flow","iccid","error_time","validate_type","validate_time","use_true_flow",
                "use_so_flow"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 YzCardInfo
     */
    public void yzcardinfo() {
        List<Map<String, Object>> channelArr = genTableMapper.YzCardInfoList(); //查询所有
        System.out.println("备份 YzCardInfo ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzCardInfo";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","vid","msisdn","iccid","imsi","open_date","activate_date","agent_id","channel_id","is_pool","pool_id","remind_ratio",
                "virtual_ratio","batch_date","syn_Time","create_by","create_date","update_by","update_date","remarks","del_flag",
                "status_id","package_id","used","remaining","imei","type","network_type","is_sms","sms_number",
                "gprs","dept_id","user_id","out_of_stock"};//数据库的注释名
        String keys[] = {"id","vid","msisdn","iccid","imsi","open_date","activate_date","agent_id","channel_id","is_pool","pool_id","remind_ratio",
                "virtual_ratio","batch_date","syn_Time","create_by","create_date","update_by","update_date","remarks","del_flag",
                "status_id","package_id","used","remaining","imei","type","network_type","is_sms","sms_number",
                "gprs","dept_id","user_id","out_of_stock"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }

    /**
     * 备份 YzCardOperating
     */
    public void yzcardoperating() {
        List<Map<String, Object>> channelArr = genTableMapper.YzCardOperatingList(); //查询所有
        System.out.println("备份 YzCardOperating ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzCardOperating";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","create_by","create_date","update_by","update_date","remarks","operating","operating_type"};//数据库的注释名
        String keys[] = {"id","create_by","create_date","update_by","update_date","remarks","operating","operating_type"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzCardPackage
     */
    public void yzcardpackage() {
        List<Map<String, Object>> channelArr = genTableMapper.YzCardPackageList(); //查询所有
        System.out.println("备份 YzCardPackage ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzCardPackage";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","package_id","package_agentname","package_name","create_time","error_so","dept_id","user_id"};//数据库的注释名
        String keys[] = {"id","package_id","package_agentname","package_name","create_time","error_so","dept_id","user_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzCardPacket
     */
    public void yzcardpacket() {
        List<Map<String, Object>> channelArr = genTableMapper.YzCardPacketList(); //查询所有
        System.out.println("备份 YzCardPacket ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzCardPacket";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","package_id","packet_id","packet_name","packet_wx_name","packet_price","packet_flow","base_packet_type","packet_type",
                "is_profit","show_profit","wechat_pay","balance_pay","create_time","error_flow","error_so","packet_valid_time","packet_cost",
                "is_month","date_limit","deduction","in_stock","packet_valid_type","dept_id","user_id","packet_valid_name"};//数据库的注释名
        String keys[] = {"id","package_id","packet_id","packet_name","packet_wx_name","packet_price","packet_flow","base_packet_type","packet_type",
                "is_profit","show_profit","wechat_pay","balance_pay","create_time","error_flow","error_so","packet_valid_time","packet_cost",
                "is_month","date_limit","deduction","in_stock","packet_valid_type","dept_id","user_id","packet_valid_name"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzCardRoute
     */
    public void yzcardroute() {
        List<Map<String, Object>> channelArr = genTableMapper.YzCardRouteList(); //查询所有
        System.out.println("备份 YzCardRoute ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzCardRoute";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"cd_id","cd_code","cd_name","cd_username","cd_pwd","cd_key","cd_url","cd_operator_type","cd_concurrency","cd_lunxun","cd_control_type",
                "cd_max_use","createTime","updateTime","cd_delete","cd_agent_id","cd_status","cd_alias"};//数据库的注释名
        String keys[] = {"cd_id","cd_code","cd_name","cd_username","cd_pwd","cd_key","cd_url","cd_operator_type","cd_concurrency","cd_lunxun","cd_control_type",
                "cd_max_use","createTime","updateTime","cd_delete","cd_agent_id","cd_status","cd_alias"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzExecutionTask
     */
    public void yzexecutiontask() {
        List<Map<String, Object>> channelArr = genTableMapper.YzExecutionTaskList(); //查询所有
        System.out.println("备份 YzExecutionTask ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzExecutionTask";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","create_time","update_time","start_time","end_time","task_name","url","auth","agent_id","type"};//数据库的注释名
        String keys[] = {"id","create_time","update_time","start_time","end_time","task_name","url","auth","agent_id","type"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzOrder
     */
    public void yzorder() {
        List<Map<String, Object>> channelArr = genTableMapper.YzOrderList(); //查询所有
        System.out.println("备份 YzOrder ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzOrder";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","ord_no","ord_type","ord_name","iccid","wx_ord_no","status","price","account","packet_id","pay_type","cre_type","is_profit",
                "add_package","show_status","open_id","agent_id","profit_type","create_time","validate_type","info","add_parameter",
                "add_package_time","del_flag","is_profit_sharing","profit_sharing_time"};//数据库的注释名
        String keys[] = {"id","ord_no","ord_type","ord_name","iccid","wx_ord_no","status","price","account","packet_id","pay_type","cre_type","is_profit",
                "add_package","show_status","open_id","agent_id","profit_type","create_time","validate_type","info","add_parameter",
                "add_package_time","del_flag","is_profit_sharing","profit_sharing_time"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzPassagewayPolling
     */
    public void yzpassagewaypolling() {
        List<Map<String, Object>> channelArr = genTableMapper.YzPassagewayPollingList(); //查询所有
        System.out.println("备份 YzPassagewayPolling ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzPassagewayPolling";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","cd_id","create_date","upd_date","cd_count","cd_current","polling_id","syn_date","polling_type"};//数据库的注释名
        String keys[] = {"id","cd_id","create_date","upd_date","cd_count","cd_current","polling_id","syn_date","polling_type"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzWechatUser
     */
    public void yzwechatuser() {
        List<Map<String, Object>> channelArr = genTableMapper.YzWechatUserList(); //查询所有
        System.out.println("备份 YzWechatUser ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzWechatUser";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","user_name","openid","head_image","iccid","sex","create_time","city","province","country","app_id","status","bind_status"};//数据库的注释名
        String keys[] = {"id","user_name","openid","head_image","iccid","sex","create_time","city","province","country","app_id","status","bind_status"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzWxAutoreply
     */
    public void yzwxautoreply() {
        List<Map<String, Object>> channelArr = genTableMapper.YzWxAutoreplyList(); //查询所有
        System.out.println("备份 YzWxAutoreply ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzWxAutoreply";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","auto_id","keywords","content","create_time","create_user","app_id"};//数据库的注释名
        String keys[] = {"id","auto_id","keywords","content","create_time","create_user","app_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzWxConfig
     */
    public void yzwxconfig() {
        List<Map<String, Object>> channelArr = genTableMapper.YzWxConfigList(); //查询所有
        System.out.println("备份 YzWxConfig ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzWxConfig";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","app_id","app_secret","app_token","origin_id","mch_id","paterner_key","index_url","back_url","status","create_time","update_time",
                "create_user","company_id","agent_id","native_url","auth_txt"};//数据库的注释名
        String keys[] = {"id","app_id","app_secret","app_token","origin_id","mch_id","paterner_key","index_url","back_url","status","create_time","update_time",
                "create_user","company_id","agent_id","native_url","auth_txt"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }


    /**
     * 备份 YzWxMenu
     */
    public void yzwxmenu() {
        List<Map<String, Object>> channelArr = genTableMapper.YzWxMenuList(); //查询所有
        System.out.println("备份 YzWxMenu ======");
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_YzWxMenu";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id","menu_id","menu_name","menu_pid","menu_url","menu_status","create_time","update_time","create_user","app_id","menu_type","media_id"};//数据库的注释名
        String keys[] = {"id","menu_id","menu_name","menu_pid","menu_url","menu_status","create_time","update_time","create_user","app_id","menu_type","media_id"};//对应的数据库字段名字
        String Flieurl = fileUrl[0] + "/" + fileUrl[1] + "/" + fileUrl[2] + "/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys, Flieurl);//导出
    }





}















































































































