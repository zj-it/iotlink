package com.yunze.system.card;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.yunze.common.core.redis.RedisCache;
import com.yunze.common.mapper.yunze.YzSysLogsMapper;
import com.yunze.common.utils.yunze.VeDate;
import com.yunze.common.utils.yunze.WriteCSV;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 导出 消费 者
 */
@Slf4j
@Component
public class BackupyzSysLogs {

    @Resource
    private RedisCache redisCache;
    @Resource
    private WriteCSV writeCSV;
    @Resource
    private YzSysLogsMapper yzSysLogsMapper;

    @RabbitHandler
    @RabbitListener(queues = "admin_BackupsyzSysLogs_queue")
    public void BkSysLogs(String msg, Channel channel) throws IOException {
        try {
            if (StringUtils.isEmpty(msg)) {
                return;
            }
            Map<String, Object> Bmap = JSON.parseObject(msg);
            //System.out.println(Bmap);
            String create_by = Bmap.get("Masage").toString();//上传新文件名
            String prefix = "admin_BackupsyzSysLogs_queue";
            //执行前判断 redis 是否存在 执行数据 存在时 不执行
            Object isExecute = redisCache.getCacheObject(prefix + ":" + create_by);
            if (isExecute == null) {
                redisCache.setCacheObject(prefix + ":" + create_by, msg, 3, TimeUnit.SECONDS);//3 秒缓存 避免 重复消费
                backups();
            }
        } catch (Exception e) {
            log.error(">>错误 - 连接管理导出 消费者:{}<<", e.getMessage().toString());
        }
    }


    /**
     * 备份 SysLogsTask
     */
    public void backups() {

        //1.查询出 yz_sys_logs
        List<Map<String, Object>> channelArr = yzSysLogsMapper.Listwhole(); //查询所有
        System.out.println("备份 SysLogsTask ======" + channelArr);
        //2 运用 WriteCSV 类 导出 csv 文件
        //VeDate获取时间类 getNo表示随机数类
        String fileName = VeDate.getNo(6) + "_SysLogsTask";
        String fileUrl[] = VeDate.getYyyyAndMm();
        String Outcolumns[] = {"id", "请求url", "创建时间", "执行的类方法", "ip", "请求参数", "响应参数", "操作人", "模块", "操作名称"};//数据库的注释名
        String keys[] = {"id", "res_url", "create_time", "class_method", "ip", "request_args", "response_args", "user_name", "project", "action_name"};//对应的数据库字段名字
        String Flieurl =  fileUrl[0]+"/"+fileUrl[1]+"/"+fileUrl[2]+"/";
        writeCSV.Write(fileName, channelArr, Outcolumns, null, keys,Flieurl);//导出
        Map<String, Object> PMap = new HashMap<>();
        PMap.put("idAll", channelArr);
        //3.根据导出的 yz_sys_logs id 去删除表数据 delete from yz_sys_logs where iccid in (1,2,3,4)
        int deleteById = yzSysLogsMapper.deleteById(PMap);
       // System.out.println("备份数据已删除======" + deleteById);
    }


    }



